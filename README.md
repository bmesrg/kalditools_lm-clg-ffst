Az `lm-clg-ffst` repo által használt Kaldis eszközöket (binárisok statikusan fordítva és a `utils` könyvtár scriptjei) tartalmazza.

Így az `lm-clg-ffst` repo használható anélkül is, hogy a teljes Kaldit (16 GB) telepíteni, fordítani kellene.